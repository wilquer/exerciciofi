#!/bin/bash

if [ $# -ne 4 ]; then
  echo "O script requer exatamente 4 nomes de arquivos como argumentos."
  exit 1
fi

lines=0
max_lines=0
max_file=""

for file in "$@"; do
  current_lines=$(wc -l < "$file")
  if [ "$current_lines" -gt "$max_lines" ]; then
    max_lines="$current_lines"
    max_file="$file"
  fi
done

cat "$max_file"

